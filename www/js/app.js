'use strict';
angular.module('radio',[
	'ui.router',
	'ui.bootstrap',
	'restangular'
]).config([
	'$stateProvider',
	'$urlRouterProvider',
	'RestangularProvider',
	function($stateProvider,$urlRouterProvider,RestangularProvider){
		$urlRouterProvider.otherwise('/');
	}
]).factory('APIRestangular',function(Restangular){
	var handler = Restangular.withConfig(function () {
  });
  handler.setRestangularFields({ id: '_id' });
  handler.setBaseUrl('api/');
  return handler;
});