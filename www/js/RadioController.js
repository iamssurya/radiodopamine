angular.module('radio').controller('RadioController',[
	'$scope',
	'$http',
	'APIRestangular',
	function($scope,$http,APIRestangular){
		console.log('Scope assigned');
		$scope.stationsList = [];
		APIRestangular.all('stations').getList('').then(function(response){
			console.log(response);
			$scope.stationsList = response;
		});
		$scope.showStationDetails = function(stationId){
			APIRestangular.all('stations').get(stationId).then(function(response){
				
			});
		}
	}
]);